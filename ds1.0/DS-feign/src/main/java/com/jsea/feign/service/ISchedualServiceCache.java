package com.jsea.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jsea.feign.service.impl.SchedualServiceCacheHystric;

@FeignClient(value="service-cache",fallback=SchedualServiceCacheHystric.class)
public interface ISchedualServiceCache {

	@RequestMapping(value = "/dataCache/clearAll",method = RequestMethod.GET)
    String clearAll();
}
