package com.jsea.feign.service.impl;

import org.springframework.stereotype.Component;

import com.jsea.feign.service.ISchedualServiceCore;

@Component
public class SchedualServiceCoreHystric implements ISchedualServiceCore{

	@Override
	public String sayHiFromClientOne(String name) {
		return "sorry "+name;
	}

}
