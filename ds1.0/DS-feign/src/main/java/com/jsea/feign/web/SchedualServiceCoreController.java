package com.jsea.feign.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsea.feign.service.ISchedualServiceCore;

@RestController
@RequestMapping(value = "")
public class SchedualServiceCoreController {

	@Autowired
    ISchedualServiceCore schedualServiceCore;
	
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    public String sayHi(@RequestParam String name){
        return schedualServiceCore.sayHiFromClientOne(name);
    }
}
