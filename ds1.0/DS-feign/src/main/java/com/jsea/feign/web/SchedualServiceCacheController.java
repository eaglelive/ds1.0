package com.jsea.feign.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jsea.feign.service.ISchedualServiceCache;

@RestController
@RequestMapping(value = "/datacache")
public class SchedualServiceCacheController {

	@Autowired
	ISchedualServiceCache schedualServiceCache;
	
    @RequestMapping(value = "clearall",method = RequestMethod.GET)
    public String clearAll(){
    	return schedualServiceCache.clearAll();
    }
}
