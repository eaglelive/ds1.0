package com.jsea.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jsea.feign.service.impl.SchedualServiceCoreHystric;

@FeignClient(value="service-core",fallback=SchedualServiceCoreHystric.class)
public interface ISchedualServiceCore {

	@RequestMapping(value = "/hi",method = RequestMethod.GET)
    String sayHiFromClientOne(@RequestParam(value = "name") String name);
}
