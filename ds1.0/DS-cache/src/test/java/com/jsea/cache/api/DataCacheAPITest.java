package com.jsea.cache.api;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
@RunWith(SpringRunner.class)
@SpringBootTest
public class DataCacheAPITest {

	/**
     * 模拟mvc测试对象
     */
    private MockMvc mockMvc;

    /**
     * web项目上下文
     */
    @Autowired
    private WebApplicationContext webApplicationContext;
	
	@Before
	public void before() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testClearAll() {
		fail("Not yet implemented");
	}

	/**
	 *  1 perform方法其实只是为了构建一个请求，并且返回ResultActions实例，该实例则是可以获取到请求的返回内容。
		2 MockMvcRequestBuilders该抽象类则是可以构建多种请求方式，如：Post、Get、Put、Delete等常用的请求方式，其中参数则是我们需要请求的本项目的相对路径，/则是项目请求的根路径。
		3 param方法用于在发送请求时携带参数，当然除了该方法还有很多其他的方法，大家可以根据实际请求情况选择调用。
		4 andReturn方法则是在发送请求后需要获取放回时调用，该方法返回MvcResult 对象，该对象可以获取到返回的视图名称、返回的Response状态、获取拦截请求的拦截器集合等。
		5 我们在这里就是使用到了第4步内的MvcResult对象实例获取的MockHttpServletResponse对象从而才得到的Status状态码。
		6 同样也是使用MvcResult实例获取的MockHttpServletResponse对象从而得到的请求返回的字符串内容。【可以查看rest返回的json数据】
		7 使用Junit内部验证类Assert判断返回的状态码是否正常为200
		8 判断返回的字符串是否与我们预计的一样。
	 * @throws Exception
	 */
	@Test
	public void testCreate() throws Exception {
		MvcResult mvcResult = mockMvc
                .perform(// 1
                        MockMvcRequestBuilders.get("/datacache/user/find/akai") // 2
//                        .param("name","admin") // 3
                )
                .andReturn();// 4

        int status = mvcResult.getResponse().getStatus(); // 5
        String responseString = mvcResult.getResponse().getContentAsString(); // 6
        System.out.println(responseString);
        Assert.assertEquals("请求错误", 200, status); // 7
        Assert.assertEquals("返回结果不一致", "name:akai	id:2	password:$2a$10$3olLFf63CHOfeprY4V77kOjFuAPXbohOse2O4BDeqAwLH9jxLb9si", responseString); // 8
	}

	@Test
	public void testGetUserByName() {
		fail("Not yet implemented");
	}

}
