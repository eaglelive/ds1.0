package com.jsea.cache.api;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jsea.cache.service.IDataCacheService;
import com.jsea.cache.service.UserService;
import com.jsea.common.dto.ResultDTO;

@RestController
@RequestMapping(value="/datacache")
public class DataCacheAPI {
	
	@Resource
	private IDataCacheService dataCacheService;
	@Resource(name="userService")
	private UserService userService;
	
	/**
	 * 清理所有缓存
	 * @return
	 */
	@RequestMapping(value="/clear/all",method = RequestMethod.GET)
	public String clearAll() {
		dataCacheService.remove();
		return "Clear Success";
	}
	
	/**
	 * 清理某名称缓存下某个key中的缓存
	 * @return
	 */
	@RequestMapping(value="/clear/{name}",method = RequestMethod.GET)
	public String clear(@PathVariable String name) {
		try {
			dataCacheService.remove(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Clear Success";
	}
	
	/**
	 * 清理某名称缓存下某个key中的缓存
	 * @return
	 */
	@RequestMapping(value="/clear/{name}/{key}",method = RequestMethod.GET)
	public String clear(@PathVariable String name,@PathVariable String key) {
		dataCacheService.remove(name, key);
		return "Clear Success";
	}
	/*@RequestMapping(value="/user/create",method = RequestMethod.GET)
	public Long create() {
		Long a = 0l;
		try {
			 a = userService.createUser(new User());
		} catch (Exception e) {
			e.printStackTrace();
			a = 0l;
		}
		return a;
	}
	@RequestMapping(value="/user/find/{name}",method = RequestMethod.GET)
	public String getUserByName(@PathVariable String name) {
		User user = userService.findUserByName(name);
		return "name:"+user.getName()+"	id:"+user.getId()+"	password:"+user.getPassword();
	}*/
	
	/**
	 * 缓存数据
	 * @param keys
	 * @param value
	 */
	@RequestMapping(value="/put",method =RequestMethod.GET)
	public void put(String[] keys , Object value){
		dataCacheService.put(keys, value);
	}
	
	/**
	 * 根据keys获取缓存中的数据
	 * @param keys
	 * @return
	 */
	@RequestMapping(value="/get",method =RequestMethod.GET)
	public Object get(String[] keys){
		return dataCacheService.get(keys);
	}
	
	/**
	 * 缓存列表
	 * @return
	 */
	@RequestMapping(value="",method =RequestMethod.GET)
	public ResultDTO getCachesName(){
		ResultDTO resDto = null;
		try {
			resDto = new ResultDTO(dataCacheService.getCacheNames());
		} catch (Exception e) {
			e.printStackTrace();
			resDto = new ResultDTO("error", "Get cache info faild...");
		}
		return resDto;
	}

	/**
	 * 所有缓存的明细信息
	 * @return
	 */
	@RequestMapping(value="/info",method =RequestMethod.GET)
	public ResultDTO getAllCachesInfo(){
		ResultDTO resDto = null;
		try {
			resDto = new ResultDTO(dataCacheService.getAllCachesInfo());
		} catch (Exception e) {
			e.printStackTrace();
			resDto = new ResultDTO("error", "Get cache info faild...");
		}
		return resDto;
	}
	
	/**
	 * 根据名称获取该缓存名下所有的缓存信息
	 * @param name
	 * @return
	 */
	@RequestMapping(value="/info/{name}",method =RequestMethod.GET)
	public ResultDTO getCacheInfoByName(@PathVariable String name){
		ResultDTO resDto = null;
		try {
			resDto = new ResultDTO(dataCacheService.getCacheInfoByName(name));
		} catch (Exception e) {
			e.printStackTrace();
			resDto = new ResultDTO("error", "Get cache info faild...");
		}
		return resDto;
	}
	
}
