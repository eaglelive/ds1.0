package com.jsea.cache.domain;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Entitys implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4147425562704230154L;

	@Override
	public String toString() {

		return ToStringBuilder.reflectionToString(this);

	}
}
