package com.jsea.cache.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jsea.cache.domain.User;

public interface UserRepository extends CrudRepository<User, Long>{

	List<User> findByName(String name);
}
