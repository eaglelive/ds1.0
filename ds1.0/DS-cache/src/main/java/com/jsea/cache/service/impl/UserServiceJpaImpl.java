package com.jsea.cache.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jsea.cache.domain.User;
import com.jsea.cache.repository.UserRepository;
import com.jsea.cache.service.IDataCacheService;
import com.jsea.cache.service.UserService;

import net.sf.ehcache.Element;

@Service(value="userService")
@Transactional(readOnly=true)
@SuppressWarnings("all")
public class UserServiceJpaImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private IDataCacheService dataCacheService;
	
	private static Logger logger = LoggerFactory.getLogger(UserServiceJpaImpl.class);
    @Override
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public Long createUser(User user) throws Exception{
        User entity = new User();
		entity.setPassword(new BCryptPasswordEncoder().encode("123"));
		entity.setName("akai"+new Date());
		userRepository.save(entity);
		if(StringUtils.isNotBlank(entity.getName())){
			throw new RuntimeException("aaaaaaaaaa");
		}
        return entity.getId();
    }

    @Override
    public User findUserByName(String name) {
        User entity = null;
        String[] cacheKey = { "TEST01","TEST"};
        if (dataCacheService.get(cacheKey) != null) {
        	entity = (User) dataCacheService.get(cacheKey);
		}else{
			entity = userRepository.findByName(name).get(0);
			dataCacheService.put(cacheKey, entity); 
		}
        getAllCacheElements();
        return entity;
    }
    @Override
    public void getStatistics() {
    	String[] cacheNames = dataCacheService.getCacheNames();
    	logger.debug(cacheNames[0]);
    }
    
    
	public List<String> getKeys() {
    	String[] cacheNames = dataCacheService.getCacheNames();
    	List<String> list = new ArrayList<String>();
    	for(String cacheName : cacheNames){
    		list.addAll(dataCacheService.getEhcacheKeys(cacheName));
    	}
    	for(String str : list){
    		logger.debug("<><><>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>:"+str);
    	}
		return list;
    }
	
	/**
	 * 获取当前所有缓存元素的监控信息
	 */
	public void getAllCacheElements(){
		String[] cacheNames = dataCacheService.getCacheNames();
		List<String> list = getKeys();
		System.out.print(StringUtils.rightPad("Key", 15));
        System.out.print(" | ");
        System.out.print(StringUtils.rightPad("Value", 25));
        System.out.print(" | ");
        System.out.print(StringUtils.rightPad("ObjectValue", 25));
        System.out.print(" | ");
        System.out.print(StringUtils.rightPad("HintCount", 10));
        System.out.print(" | ");
        System.out.print(StringUtils.rightPad("CreationTime", 25));
        System.out.print(" | ");
        System.out.print(StringUtils.rightPad("LastAccessTime", 25));
        System.out.print(" | ");
        System.out.print(StringUtils.rightPad("TimeToLive(ms)", 15));
        System.out.print(" | ");
        System.out.print(StringUtils.rightPad("TimeToIdle(ms)", 15));
        System.out.print(" | ");
        System.out.println();
		for(String cacheName : cacheNames){
			for(String key : list){
				Element element = dataCacheService.getElements(cacheName, key);
				if(element!=null){
					System.out.print(StringUtils.rightPad(key, 15));//key name
		            System.out.print(" | ");
		            System.out.print(StringUtils.rightPad(""+element.getValue(), 15));//Value(弃用)
		            System.out.print(" | ");
		            System.out.print(StringUtils.rightPad(""+element.getObjectValue(), 15));//ObjectValue
		            System.out.print(" | ");
		            System.out.print(StringUtils.rightPad(""+element.getHitCount(), 10));//命中次数
		            System.out.print(" | ");
		            System.out.print(StringUtils.rightPad(pareTime(element.getCreationTime()), 25));//创建时间
		            System.out.print(" | ");
		            System.out.print(StringUtils.rightPad(pareTime(element.getLastAccessTime()), 25));//最后访问时间
		            System.out.print(" | ");
		            System.out.print(StringUtils.rightPad(""+element.getTimeToLive(), 15));   //存活时间
		            System.out.print(" | ");
		            System.out.print(StringUtils.rightPad(""+element.getTimeToIdle(), 15));   //空闲时间
		            System.out.print(" | ");
		            System.out.println();
				}
			}
		}
	}
	
	private static String pareTime(long millis){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(millis);

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            return formatter.format(calendar.getTime());
        } catch (Exception e) {
            logger.error("",e);
        }
        return null;
    }

}
