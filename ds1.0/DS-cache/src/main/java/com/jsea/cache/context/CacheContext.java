package com.jsea.cache.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheContext {
	
	Logger logger = LoggerFactory.getLogger(CacheContext.class);
	
	private static CacheContext context = new CacheContext();
	private boolean isEnable = true;
	
	private CacheContext() {
	}
	
	public static CacheContext getInstance() {
		return context;
	}
	
	public void disable() {
		logger.info("System cache service disable .");
		this.isEnable = false;
	}
	
	public void enable() {
		logger.info("System cache service enable .");
		this.isEnable = true;
	}
	
	public boolean isEnable() {
		return this.isEnable;
	}
}
