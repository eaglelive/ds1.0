package com.jsea.cache.ehcache.register;

import org.springframework.stereotype.Component;

import com.jsea.cache.context.CacheContext;
import com.jsea.cache.ehcache.service.HqCacheManager;
@Component
public class ZkConfigRegister {

	static {
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static void init() throws Exception {
		String rmiIP = "127.0.0.1";
		String rmiPort = "40001";
		
		CacheContext.getInstance().enable();

		HqCacheManager cacheManager = new HqCacheManager();
		cacheManager.create(rmiIP, rmiPort);
	}

}
