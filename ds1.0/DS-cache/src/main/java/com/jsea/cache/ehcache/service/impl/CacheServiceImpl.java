package com.jsea.cache.ehcache.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.jsea.cache.context.CacheContext;
import com.jsea.cache.ehcache.service.HqCacheManager;
import com.jsea.cache.ehcache.service.ICacheService;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

@Service("hqCacheService")
@SuppressWarnings("all")
public class CacheServiceImpl implements ICacheService {
    
	Logger logger = LoggerFactory.getLogger(CacheServiceImpl.class.getName());
    
    private CacheManager cacheManager;
    
    protected CacheManager getCacheManager() {
        if (cacheManager == null) {
            cacheManager = CacheManager.getCacheManager(HqCacheManager.HQ_CACHE_MANAGER);
        }
        return cacheManager;
    }
    
    protected Cache getCache() {
        return getCacheManager().getCache(HqCacheManager.HQ_CACHE_DATA);
    }
    
    protected Element getElement(String key) {
        Element element = getCache().get(key);
        if (element == null) {
            element = new Element(key, new HashMap<String, Object>());
            getCache().put(element);
        }
        return element;
    }

    @Override
    public void put(String key, Object value) {
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return;
    	}
        if (value == null) {
            value = new HashMap<String, Object>();
        }
        getCache().put(new Element(key, value));
        logger.debug(key + " put cache.");
    }
    
    @Override
    public Element get(String key) {
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return null;
    	}
        return getElement(key);
    }
    
    @Override
    public Map<String, Object> getData(String key) {
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return null;
    	}
        return (Map<String, Object>)getElement(key).getObjectValue();
    }
    
    @Override
    public void remove() {
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return;
    	}
        getCache().removeAll();
        logger.debug("Clear all cache.");
    }

    /*
     * 根据名称获取缓存对象
     * @see com.jsea.cache.ehcache.service.ICacheService#getEhcache(java.lang.String)
     */
    public Ehcache getEhcache(String name){
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return null;
    	}
        return cacheManager.getEhcache(name);
    }
    
    /*
     * 获取缓存名中所有的key
     * @see com.jsea.cache.ehcache.service.ICacheService#getEhcacheKeys(java.lang.String)
     */
    public List getEhcacheKeys(String name){
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return null;
    	}
        return getEhcache(name).getKeys();
    }
    
    /*	
     * 根据缓存名和key获取Element对象
     * (non-Javadoc)
     * @see com.jsea.cache.ehcache.service.ICacheService#getElements(java.lang.String, java.lang.String)
     */
    public Element getElements(String name,String key){
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return null;
    	}
        Ehcache cache = getEhcache(name);
        if(cache == null){
            return null;
        }
        return cache.getQuiet(key);
    }
    
    /*
     * 获取所有的缓存对象
     * @see com.jsea.cache.ehcache.service.ICacheService#getEhCaches()
     */
    public List<Ehcache> getEhCaches(){
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return null;
    	}
        String[] cacheNames = getCacheNames();
        List<Ehcache> list = new ArrayList<Ehcache>();
        for(String cacheName:cacheNames){
            list.add(getEhcache(cacheName));
        }
        return list;
    }
    
    /*
     * 获取所有缓存的名称
     * @see com.jsea.cache.ehcache.service.ICacheService#getCacheNames()
     */
    public String[] getCacheNames(){
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return null;
    	}
        return cacheManager.getCacheNames();
    }


    /*
     * 根据名称和key清除缓存
     * @see com.jsea.cache.ehcache.service.ICacheService#removeByNameAndKey(java.lang.String, java.lang.String)
     */
    @Override
    public boolean remove(String name,String key){
    	if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return false;
    	}
    	boolean flag = false;
    	Ehcache ehcache = getEhcache(name);
    	if(ehcache!=null){
    		flag = ehcache.remove(key);
    	}
    	return flag;
    }

    /*
     * 根据名称清除缓存
     * @see com.jsea.cache.ehcache.service.ICacheService#remove(java.lang.String)
     */
	@Override
	public void remove(String name) {
		if (!CacheContext.getInstance().isEnable()) {
    		logger.debug("cache disable .... ");
    		return;
    	}
		getCacheManager().getCache(name).removeAll();
	}
    
}
