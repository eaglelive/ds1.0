/**
 * @Title: CacheKeyProvider.java
 * @Copyright (C) 2016 太极华青
 * @Description:
 * @Revision 1.0 2016-6-1  CAOK
 */

package com.jsea.cache.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName: CacheKeyProvider
 * @Description: ����KEY����
 * @author: CAOK 2016-6-1 ����01:34:36 Modify 2017-10-27 10:33
 */
@Service
@Transactional(readOnly = true)
public class CacheKeyProvider implements ICacheKeyProvider {

	Logger logger = LoggerFactory.getLogger(CacheKeyProvider.class);

    /**
     * .
     * <p>
     * Title: getNewKeys
     * </p>
     * <p>
     * Description:
     * </p>
     * @param oldKeys
     * @return
     * @see com.tjhq.commons.cache.utils.ICacheKeyProvider#getNewKeys(java.lang.String[])
     */
    @Override
    public String[] getNewKeys(String[] oldKeys) {
        String[] newKeys = new String[oldKeys.length + 2];
        newKeys[0] = "FRIM";
        newKeys[1] = "2020";

        for (int i = 0; i < oldKeys.length; i++) {
            newKeys[i + 2] = oldKeys[i];
        }
        return newKeys;
    }

}
