package com.jsea.cache.service;

public interface IDataChangeListener {

    void update(Object data);
}
