package com.jsea.cache.ehcache.service;

import java.util.List;
import java.util.Map;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
@SuppressWarnings("all")
public interface ICacheService {
    
    public void put(String key, Object value);
    
    public Element get(String key);
    
    public Map<String, Object> getData(String key);
    
    public void remove();
    
    /**
     * 根据名称获取缓存对象
     * @param name
     * @return
     */
    public Ehcache getEhcache(String name);
    
    /**
     * 获取缓存名中所有的key
     * @param name
     * @return
     */
    public List getEhcacheKeys(String name);
    /**
     * 根据缓存名和key获取Element对象
     * @param name
     * @param key
     * @return
     */
    public Element getElements(String name,String key);
    
    /**
     * 获取所有的缓存对象
     * @return
     */
    public List<Ehcache> getEhCaches();
    
    /**
     * 获取所有缓存的名称
     * @return
     */
    public String[] getCacheNames();

    /**
     * 根据名称和key清除缓存
     * @param name
     * @param key
     * @return
     */
	boolean remove(String name, String key);
	
	/**
     * 根据名称清除缓存
     * @param name
     * @return
     */
	public void remove(String name);
}
