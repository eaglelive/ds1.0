package com.jsea.cache.service;


import com.jsea.cache.domain.User;

public interface UserService {

    Long createUser(User user) throws Exception;

    User findUserByName(String name);

	void getStatistics();

}
