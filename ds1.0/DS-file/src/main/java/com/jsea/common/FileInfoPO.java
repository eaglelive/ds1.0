package com.jsea.common;

import java.io.Serializable;

public class FileInfoPO implements Serializable {

	private static final long serialVersionUID = 8568030645884119446L;
	private String fileID;
	private String userID;
	private String fileName;
	private String filePath;
	private long fileSize;
	private String storageType;

	public FileInfoPO() {
	}

	public FileInfoPO(String fileID, String userID, String storageType,
			String fileName, String filePath, long fileSize) {
		this.fileID = fileID;
		this.userID = userID;
		this.storageType = storageType;
		this.fileName = fileName;
		this.filePath = filePath;
		this.fileSize = fileSize;
	}

	/**
	 * @return the fileID
	 */
	public String getFileID() {
		return fileID;
	}

	/**
	 * @param fileID the fileID to set
	 */
	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	/**
	 * @return the userID
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the fileSize
	 */
	public long getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the storageType
	 */
	public String getStorageType() {
		return storageType;
	}

	/**
	 * @param storageType the storageType to set
	 */
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	
	

	

}

