package com.jsea.common;

import java.util.UUID;

public class Tools {
	
	public static String getGUID() {
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
}
