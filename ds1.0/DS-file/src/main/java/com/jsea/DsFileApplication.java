package com.jsea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsFileApplication.class, args);
	}
}
