package com.jsea.server.local.utils;

import java.util.Calendar;

import org.springframework.util.StringUtils;

public class Tools {

	private static String rootPath = "webfile";

	/**
	 * FILE_ROOT_DIR/2017/01/userID/fileName
	 * 
	 * @param fileName
	 * @param userID
	 * @return
	 */
	public static String getFilePath(String fileName) {
		if (StringUtils.isEmpty(fileName)) {
			return "";
		}
		Calendar now = Calendar.getInstance();
		StringBuffer filePath = new StringBuffer();
		filePath.append(rootPath).append("/");
		filePath.append(now.get(Calendar.YEAR)).append("/");
		filePath.append(now.get(Calendar.MONTH) + 1).append("/");
		filePath.append(now.get(Calendar.DAY_OF_MONTH)).append("/");
		filePath.append(fileName);

		return filePath.toString();
	}
}
