package com.jsea.server;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface IFileServer {
	
	String upload(MultipartFile file) throws IOException;
	File download(String filePath);
	
}
