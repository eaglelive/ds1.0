package com.jsea.store.service;

import com.jsea.common.FileInfoPO;

public interface IDataStoreService {
	
	void saveFile(FileInfoPO fileInfo);
	FileInfoPO findFile(String fileID);
}
