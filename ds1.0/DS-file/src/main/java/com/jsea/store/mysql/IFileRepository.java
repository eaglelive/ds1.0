/**
 * 
 */
package com.jsea.store.mysql;

import org.springframework.data.repository.CrudRepository;

import com.jsea.store.mysql.entity.FileEntity;

/**
 * @author CAOK
 *
 */
public interface IFileRepository extends CrudRepository<FileEntity, String> {

}
