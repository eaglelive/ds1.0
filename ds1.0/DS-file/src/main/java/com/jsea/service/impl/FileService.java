package com.jsea.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.jsea.common.FileInfoPO;
import com.jsea.common.Tools;
import com.jsea.server.IFileServer;
import com.jsea.service.IFileService;
import com.jsea.store.service.IDataStoreService;

@Service
public class FileService implements IFileService {
	
	@Resource(name="localFileServer")
	IFileServer fileServer;
	
	@Resource(name="dataStoreService")
	IDataStoreService dataStoreService;

	@Override
	public FileInfoPO upload(MultipartFile file, String userID) throws Exception {
		if (file == null || file.isEmpty()) {
			throw new Exception("上传的文件为空！");
		}
		
		FileInfoPO fileInfo = new FileInfoPO();
		fileInfo.setFileID(Tools.getGUID());
		fileInfo.setUserID(userID);
		fileInfo.setFileName(file.getOriginalFilename());
		fileInfo.setStorageType("local");
		fileInfo.setFileSize(file.getSize());
		
		fileInfo.setFilePath(fileServer.upload(file));
		
		dataStoreService.saveFile(fileInfo);
		
		return fileInfo;
	}

	@Override
	public FileInfoPO download(String fileID) {
		FileInfoPO file = dataStoreService.findFile(fileID);
		return file;
	}

}
