package com.jsea.service;

import org.springframework.web.multipart.MultipartFile;

import com.jsea.common.FileInfoPO;

public interface IFileService {
	
	FileInfoPO upload(MultipartFile file, String userID) throws Exception;
	FileInfoPO download(String fileID);
	

}
