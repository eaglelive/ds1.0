package com.jsea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsTopicApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsTopicApplication.class, args);
	}
}
